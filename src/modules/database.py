import sqlite3
from flask import jsonify
#import psycopg2
import os

class Database(object):
    instCount = 0
    def __init__(self):
        self.db = sqlite3.connect('database.db')
        self.cursor = self.db.cursor()
        self.instCount += 1
        print(u"Instância: {0}".format(self.instCount))

class TryQuery(Database):
    def __init__(self, query):
        Database.__init__(self)
        self.query = query

    def jsonSelect(self):
        try:
            self.cursor.execute(self.query)
            self.db.commit()
            return {"isOK":"1", "RESPONSE":self.cursor.fetchall()}
        except Exception as e:
            return {"isOK":"0", "QUERY":self.query, "ERROR":"{0}".format(e)}

    def jsonCommit(self):
        try:
            self.cursor.execute(self.query)
            self.db.commit()
            return {"isOK":"1"}
        except Exception as e:
                return {"isOK":"0", "QUERY":self.query, "ERROR":"{0}".format(e)}

class DbBond(Database):
    def __init__(self):
        Database.__init__(self)

    def getBondsOfAuthor(self, author_id):#Retorna as obras do autor
        query = "SELECT art_id, art.name FROM authorArt INNER JOIN art ON art_id=art.id where author_id={0}".format(author_id)
        tquery = TryQuery(query)
        return tquery.jsonSelect()

    def getBondsOfArt(self, art_id):#retorna os autores da arte
        query = "SELECT author_id, author.name FROM authorArt INNER JOIN author ON author_id=author.id WHERE art_id={0}".format(art_id)
        tquery = TryQuery(query)
        return tquery.jsonSelect()

    def addBond(self, author_id, arts_ids):#Adiciona autor as artes
        self.removeBondsOfAuthor(author_id)
        query = "INSERT INTO authorArt(author_id, art_id) VALUES "
        print("LEN ARTS_IDS {0}".format(len(arts_ids)))
        print(arts_ids)
        for art_id in arts_ids:
            
            if (art_id.isdigit()):
                print("art_id: {0} {1}".format(art_id, art_id.isdigit))
                query += "({0}, {1}),".format(author_id, art_id)
        query = query[:-1]
        tquery = TryQuery(query)
        return tquery.jsonCommit()

    def removeBondOfAuthor(self, author_id, art_id):#Remove o vinculo de autor com obra
        query = "DELETE FROM authorArt WHERE author_id={0} and art_id={1}".format(author_id, art_id)
        tquery = TryQuery(query)
        return tquery.jsonCommit()

    def removeBondsOfAuthor(self, author_id):#Remove o vinculo de autor com todas as obras
        query = "DELETE FROM authorArt WHERE author_id={0}".format(author_id)
        tquery = TryQuery(query)
        return tquery.jsonCommit()

    def removeBondOfArt(self, art_id):#Remove todos os vinculos relacionados a arte
        query = "DELETE FROM authorArt WHERE and art_id={0}".format(art_id)
        tquery = TryQuery(query)
        return tquery.jsonCommit()

class DbCountry(Database):
    def __init__(self):
        Database.__init__(self)

    def get(self):
        query = "SELECT id, name_pt from country"
        tquery = TryQuery(query)
        return tquery.jsonSelect()

class DbAuthor(Database):
    def __init__(self):
        Database.__init__(self)

    def add(self, name, genre_id, email, burnDate, country_id, cpf):
        query = "INSERT INTO author(name, genre_id, email, burnDate, country_id, cpf)\
            VALUES ('{0}', {1}, '{2}', '{3}', {4}, '{5}')".format(name, genre_id, email, burnDate, country_id, cpf)
        tquery = TryQuery(query)
        return tquery.jsonCommit()

    def get(self, author_id=None, limit=None, offset=None):
        query = "SELECT id, name, genre_id, email, burnDate, country_id, cpf FROM author"
        if author_id:
            query += " WHERE author.id={0}".format(author_id)
        query += " ORDER BY author.id DESC"
        if limit:
            query += " LIMIT {0}".format(limit)
        if offset:
            query += " OFFSET {0}".format(offset)
        tquery = TryQuery(query)
        return tquery.jsonSelect()

    def change(self, author_id, name, genre_id, email, burnDate, country_id, cpf):
        query = "UPDATE author set name='{0}', genre_id='{1}', email='{2}', burnDate='{3}', country_id='{4}', cpf='{5}'\
            WHERE id={6}".format(name, genre_id, email, burnDate, country_id, cpf, author_id)
        tquery = TryQuery(query)
        return tquery.jsonCommit()

    def remove(self, author_id):
        query = "DELETE FROM author WHERE id={0}".format(author_id)
        tquery = TryQuery(query)
        return tquery.jsonCommit()

    def getFromEmail(self, email):
        query = "SELECT id, name FROM author WHERE email='{0}'".format(email)
        tquery = TryQuery(query)
        return tquery.jsonSelect()

class DbArt(Database):
    def __init__(self):
        Database.__init__(self)

    def add(self, name, description, publicationDate, exposureDate):
        query = "INSERT INTO art(name, description, publicationDate, exposureDate)\
            VALUES ('{0}', '{1}', '{2}', '{3}')".format(name, description, publicationDate, exposureDate)
        tquery = TryQuery(query)
        return tquery.jsonCommit()

    def get(self, art_id=None, limit=None, offset=None):
        query = "SELECT id, name, description, publicationDate, exposureDate FROM art"
        if art_id:
            query += " WHERE art.id={0}".format(art_id)
        query += " ORDER BY art.id DESC"
        if limit:
            query += " LIMIT {0}".format(limit)
        if offset:
            query += " OFFSET {0}".format(offset)
        tquery = TryQuery(query)
        return tquery.jsonSelect()

    def change(self, art_id, name, description, publicationDate, exposureDate):
        query = "UPDATE art set name='{0}', description='{1}', publicationDate='{2}', exposureDate='{3}'\
            WHERE id={4}".format(name, description, publicationDate, exposureDate, art_id)
        tquery = TryQuery(query)
        return tquery.jsonCommit()

    def remove(self, art_id):
        query = "DELETE FROM art WHERE id={0}".format(art_id)
        tquery = TryQuery(query)
        return tquery.jsonCommit()



if __name__ == '__main__':
    print (os.getcwd())
    # os.chdir('../../')
    
    activator = 'src/environment/scripts/activate_this.py'#Ativacao de ambiente de execucao
    with open(activator) as f:
        exec(f.read(), {'__file__': activator})


    dbAuthor = DbAuthor()
    dbArt = DbArt()
    dbAuthor.add('Israel Gomes', 1, 'blender@test.com', '1994-04-10', 1, '109.320.864-38')
    #dbAuthor.change(2, 'testAlterado', 1, 'test@test.com.br', '1994-04-10', 'Brasil', '109.320.864-38')
    print(dbAuthor.get(limit=1))
    #dbAuthor.remove(1)
    print(dbAuthor)



    # host="ec2-54-80-184-43.compute-1.amazonaws.com"
    # user="firfcfkyedvdqc"
    # password="570cdfeec753e984d526042235e87240168ae1302ebfd56d15acbe4530bc80a3"
    # database="ddlc44pjs9soer"

    # db = psycopg2.connect(
    #     database=database,
    #     user=user,
    #     password=password,
    #     host=host,
    #     port=5432
    # )
    # db.autocommit = True

    print




