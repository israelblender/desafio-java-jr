if (!String.prototype.format) {
    String.prototype.format = function() {
      var args = arguments;
      return this.replace(/{(\d+)}/g, function(match, number) { 
          return typeof args[number] != 'undefined'? args[number] : match;
      });
    };
  }

function print(element){
    console.log(element);
}
function encodeByDict(dict){
    string = "";
    for (key in dict){
        string += key+"="+dict[key]+"&";
    }
    return string.substring(0, string.length-1);
}

function len(element){
    if (typeof element == "object"){
        var length = 0;
        for (item in element){
            length += 1;
        }
    }else{var length = element.length}
    return length;
}
function formatDate(dateString){
    if (dateString.indexOf('-') > -1){
        let dateSplit = dateString.split('-');
        var dateResult = [dateSplit[2], dateSplit[1], dateSplit[0]].join('/')
    }else{
        let dateSplit = dateString.split('/');
        var dateResult = [dateSplit[2], dateSplit[1], dateSplit[0]].join('-')
    }
    return dateResult;
}
function validatedate(inputText)
  {
  var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
  // Match the date format through regular expression
  if(inputText.value.match(dateformat))
  {
  document.form1.text1.focus();
  //Test which seperator is used '/' or '-'
  var opera1 = inputText.value.split('/');
  var opera2 = inputText.value.split('-');
  lopera1 = opera1.length;
  lopera2 = opera2.length;
  // Extract the string into month, date and year
  if (lopera1>1)
  {
  var pdate = inputText.value.split('/');
  }
  else if (lopera2>1)
  {
  var pdate = inputText.value.split('-');
  }
  var dd = parseInt(pdate[0]);
  var mm  = parseInt(pdate[1]);
  var yy = parseInt(pdate[2]);
  // Create list of days of a month [assume there is no leap year by default]
  var ListofDays = [31,28,31,30,31,30,31,31,30,31,30,31];
  if (mm==1 || mm>2)
  {
  if (dd>ListofDays[mm-1])
  {
  alert('Invalid date format!');
  return false;
  }
  }
  if (mm==2)
  {
  var lyear = false;
  if ( (!(yy % 4) && yy % 100) || !(yy % 400)) 
  {
  lyear = true;
  }
  if ((lyear==false) && (dd>=29))
  {
  alert('Invalid date format!');
  return false;
  }
  if ((lyear==true) && (dd>29))
  {
  alert('Invalid date format!');
  return false;
  }
  }
  }
  else
  {
  alert("Invalid date format!");
  document.form1.text1.focus();
  return false;
  }
  }


function setTimeoutTryCorrectAnswer(){
    var callbackSuccess = this.callbackSuccess;
    var callbackError = this.callbackError;
    var idRequest = this.idRequest+"_"+this.options[this.nameVar]
    METHODS[this.method](this.url, this.params, this.options, this.headers
    , function(response, options){//CallbackSuccess
        //Limpa as variaveis responsaveis por gravar falha de carregamento de página ou de requisições de rede
        options.TRY_REQUEST = 0;
        options.TRY_CONTENT = 0;
        callbackSuccess(response, options);
    }, function(response, options){//CallbackError
        callbackError(response, options);
    }, this.callbacksValidation, this.attempts, this.timeSleepAttempt, this.functionMaster, idRequest);
}
//Funcao chamada para realizar tentativas de requisicao caso haja falha no carregamento da página ou da requisição
function TryCorrectAnswer(method, nameVar, url, params, lastResponse, options=false, headers=false, callbackSuccess=null, callbackError=null, callbacksValidation=null, attempts=3, timeSleepAttempt=1000, functionMaster="", idRequest=null){
    this.method = method;
    this.nameVar = nameVar;
    this.url = url;
    this.params = params;
    this.lastResponse = lastResponse;
    this.options = options;
    this.headers = headers;
    this.callbackSuccess = callbackSuccess;
    this.callbackError = callbackError;
    this.callbacksValidation = callbacksValidation;
    this.attempts = attempts;
    this.timeSleepAttempt = timeSleepAttempt;
    this.functionMaster = functionMaster;
    this.idRequest = idRequest;
    
    if (!this.options[nameVar]){//Define elementos necessários para realização de tentativas caso a requisicao falhe
        this.options[nameVar] = 1;//Index de tentativas
        this.options.ATTEMPTS = attempts;//Quantidade de tentativas
        this.options.TIME_SLEEP_ATTEMPT = timeSleepAttempt;//Quantidade de milisegundos
    }
    this.options[nameVar] += 1;//Incrementa uma tentativa a variável de tentativas
    if (this.options[nameVar] == this.options.ATTEMPTS){//Caso seja feita mais de options.ATTEMPTS tentativas, retorna erro
        this.options.FUNCTION_ERROR = "ERRO {0} Realizado {1} tentativas".format(functionMaster, this.options.ATTEMPTS);
        callbackError(this.lastResponse, this.options);
    }else{//Realiza tentativa de acessar a página até a quinta tentativa
        print("{0} Realizando {1} ª tentativa [Error Request]".format(functionMaster, String(this.options[nameVar]-1)));
        setTimeout(setTimeoutTryCorrectAnswer.bind(this), this.options.TIME_SLEEP_ATTEMPT);
    }
}
function POST(url, params, options=false, headers=false, callbackSuccess=null, callbackError=null, callbacksValidation={}, attempts=3, timeSleepAttempt=1000){

    var http = new XMLHttpRequest();
    http.open('POST', url, true);

    if (headers){
        for (key in headers){
            http.setRequestHeader(key, headers[key]);
        }
    }
    if ( !headers || (headers && !headers.hasOwnProperty('Accept')) ){
        http.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
    }
    if ( !headers || (headers && !headers.hasOwnProperty('Accept-Language')) ){
        http.setRequestHeader('Accept-Language', 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7');
    }
    http.onreadystatechange = function(){
        if(http.readyState == 4) {
            setTimeout(function(){
                if (http.status == 200){//Se igual a 200
                    if (callbacksValidation && callbacksValidation["success"]){
                        var callVal = callbacksValidation["success"](http.responseText, options);
                        if (callVal == true){//Se retornar true, prossegue com chamada positiva
                            callbackSuccess(http.responseText, options);
                        }else{
                            if (callVal == false){//Se chamada for false, prossegue com nova tentativa
                                new TryCorrectAnswer("POST", "TRY_CONTENT", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                            }else{
                                //callbackSuccess é definido na própria interface da chamada do POST
                            }
                        }
                    }else{
                        callbackSuccess(http.responseText, options);
                    }
                }
                else{//Se diferente de 200
                    if (callbacksValidation && callbacksValidation["error"]){//Caso callbackValidation tenha sido definida
                        var callVal = callbacksValidation["error"](http.responseText, options);//Chama callbackValidation
                        if (callVal == true){//Se retornar true, prossegue com chamada positiva
                            callbackError(http.responseText, options);
                        }else{
                            if (callVal == false){//Se chamada for false, prossegue com nova tentativa
                                new TryCorrectAnswer("POST", "TRY_CONTENT", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                            }else{
                                //callbackError é definido na própria interface da chamada do POST
                            }
                        }
                    }else{//Se ocorreu erro na página e não foi definido validation
                        new TryCorrectAnswer("POST", "TRY_REQUEST", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                    }
                }
            }, 300);
        }
    }
    try{
        http.send(params)
    }catch(e){console.log("Erro ao realizar download.")}
}
function PUT(url, params, options=false, headers=false, callbackSuccess=null, callbackError=null, callbacksValidation={}, attempts=3, timeSleepAttempt=1000){

    var http = new XMLHttpRequest();
    http.open('PUT', url, true);

    if (headers){
        for (key in headers){
            http.setRequestHeader(key, headers[key]);
        }
    }
    if ( !headers || (headers && !headers.hasOwnProperty('Accept')) ){
        http.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
    }
    if ( !headers || (headers && !headers.hasOwnProperty('Accept-Language')) ){
        http.setRequestHeader('Accept-Language', 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7');
    }
    http.onreadystatechange = function(){
        if(http.readyState == 4) {
            setTimeout(function(){
                if (http.status == 200){//Se igual a 200
                    if (callbacksValidation && callbacksValidation["success"]){
                        var callVal = callbacksValidation["success"](http.responseText, options);
                        if (callVal == true){//Se retornar true, prossegue com chamada positiva
                            callbackSuccess(http.responseText, options);
                        }else{
                            if (callVal == false){//Se chamada for false, prossegue com nova tentativa
                                new TryCorrectAnswer("PUT", "TRY_CONTENT", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                            }else{
                                //callbackSuccess é definido na própria interface da chamada do POST
                            }
                        }
                    }else{
                        callbackSuccess(http.responseText, options);
                    }
                }
                else{//Se diferente de 200
                    if (callbacksValidation && callbacksValidation["error"]){//Caso callbackValidation tenha sido definida
                        var callVal = callbacksValidation["error"](http.responseText, options);//Chama callbackValidation
                        if (callVal == true){//Se retornar true, prossegue com chamada positiva
                            callbackError(http.responseText, options);
                        }else{
                            if (callVal == false){//Se chamada for false, prossegue com nova tentativa
                                new TryCorrectAnswer("PUT", "TRY_CONTENT", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                            }else{
                                //callbackError é definido na própria interface da chamada do POST
                            }
                        }
                    }else{//Se ocorreu erro na página e não foi definido validation
                        new TryCorrectAnswer("PUT", "TRY_REQUEST", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                    }
                }
            }, 300);
        }
    }
    try{
        http.send(params)
    }catch(e){console.log("Erro ao realizar download.")}
}
function DELETE(url, params, options=false, headers=false, callbackSuccess=null, callbackError=null, callbacksValidation={}, attempts=3, timeSleepAttempt=1000){

    var http = new XMLHttpRequest();
    http.open('DELETE', url, true);

    if (headers){
        for (key in headers){
            http.setRequestHeader(key, headers[key]);
        }
    }
    if ( !headers || (headers && !headers.hasOwnProperty('Accept')) ){
        http.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
    }
    if ( !headers || (headers && !headers.hasOwnProperty('Accept-Language')) ){
        http.setRequestHeader('Accept-Language', 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7');
    }
    http.onreadystatechange = function(){
        if(http.readyState == 4) {
            setTimeout(function(){
                if (http.status == 200){//Se igual a 200
                    if (callbacksValidation && callbacksValidation["success"]){
                        var callVal = callbacksValidation["success"](http.responseText, options);
                        if (callVal == true){//Se retornar true, prossegue com chamada positiva
                            callbackSuccess(http.responseText, options);
                        }else{
                            if (callVal == false){//Se chamada for false, prossegue com nova tentativa
                                new TryCorrectAnswer("DELETE", "TRY_CONTENT", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                            }else{
                                //callbackSuccess é definido na própria interface da chamada do POST
                            }
                        }
                    }else{
                        callbackSuccess(http.responseText, options);
                    }
                }
                else{//Se diferente de 200
                    if (callbacksValidation && callbacksValidation["error"]){//Caso callbackValidation tenha sido definida
                        var callVal = callbacksValidation["error"](http.responseText, options);//Chama callbackValidation
                        if (callVal == true){//Se retornar true, prossegue com chamada positiva
                            callbackError(http.responseText, options);
                        }else{
                            if (callVal == false){//Se chamada for false, prossegue com nova tentativa
                                new TryCorrectAnswer("DELETE", "TRY_CONTENT", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                            }else{
                                //callbackError é definido na própria interface da chamada do POST
                            }
                        }
                    }else{//Se ocorreu erro na página e não foi definido validation
                        new TryCorrectAnswer("DELETE", "TRY_REQUEST", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                    }
                }
            }, 300);
        }
    }
    try{
        http.send(params)
    }catch(e){console.log("Erro ao realizar download.")}
}
function GET(url, params=null, options=false, headers, callbackSuccess=null, callbackError=null, callbacksValidation={}, attempts=3, timeSleepAttempt=1000){
    var http = new XMLHttpRequest();
    http.open('GET', url+(params?"/"+params:""), true);
    http.timeout = 15000; // time in milliseconds
    if (headers){
        for (key in headers){
            http.setRequestHeader(key, headers[key]);
        }
    }
    if ( !headers || (headers && !headers.hasOwnProperty('Accept')) ){
        http.setRequestHeader('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8');
    }
    if ( !headers || (headers && !headers.hasOwnProperty('Accept-Language')) ){
        http.setRequestHeader('Accept-Language', 'pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7');
    }

    http.onreadystatechange = function() {
        if(http.readyState == 4) {//Dispute is invalid as a dispute already exists for the full transaction amount of this transaction
            // IH.removeRequestOpened();
            setTimeout(function(){
                if (http.status == 200){//Se igual a 200
                    if (typeof callbacksValidation == "function"){
                        throw "CALLBACKVALIDATION DEVE SER ARRAY CONTENDO success e error";
                    }
                    if (callbacksValidation && callbacksValidation["success"]){
                        var callVal = callbacksValidation["success"](http.responseText, options);
                        if (callVal == true){//Se retornar true, prossegue com chamada positiva
                            callbackSuccess(http.responseText, options);
                        }else{
                            if (callVal == false){//Se chamada for false, prossegue com nova tentativa
                                new TryCorrectAnswer("GET", "TRY_CONTENT", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                            }else{
                                //callbackSuccess é definido na própria interface da chamada do POST
                            }
                        }
                    }else{
                        callbackSuccess(http.responseText, options);
                    }
                }
                else{//Se diferente de 200
                    if (http.responseText.indexOf("expirou") > 0){//Se página SAT estiver deslogada
                        callbackError(http.responseText, options);
                    }else{
                        if (callbacksValidation && callbacksValidation["error"]){//Caso callbackValidation tenha sido definida
                            var callVal = callbacksValidation["error"](http.responseText, options);//Chama callbackValidation
                            if (callVal == true){//Se retornar true, prossegue com chamada positiva
                                callbackSuccess(http.responseText, options);
                            }else{
                                if (callVal == false){//Se chamada for false, prossegue com nova tentativa
                                    new TryCorrectAnswer("GET", "TRY_CONTENT", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                                }else{
                                    //callbackError é definido na própria interface da chamada do POST
                                }
                            }
                        }else{//Se ocorreu erro na página e não foi definido validation
                            new TryCorrectAnswer("GET", "TRY_REQUEST", url, params, http.responseText, options, headers, callbackSuccess, callbackError, callbacksValidation, attempts, timeSleepAttempt);
                        }
                    }
                }
            }, 300);
        }
    }
    try{
        http.send()
    }catch(e){console.log("Erro ao realizar download.")}
}
METHODS = {'POST':POST, 'GET':GET, 'PUT':PUT, 'DELETE':DELETE};