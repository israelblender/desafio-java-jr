class CountrysRequest{
    constructor(){
    }
    get(callbackSuccess, callbackError){
        var url = '/countrys';
        GET(url, null, {}, {}, 
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError, null, 3, 1000);
    }
}

selfBond = null;
class BondRequest{
    constructor(){
        selfBond = this;
    }
    getBondsOfAuthor(author_id, callbackSuccess, callbackError){
        var url = '/bondsofauthor/'+author_id;
        GET(url, null, {}, {},
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError);
    }
    getBondsOfArt(art_id, callbackSuccess, callbackError){
        var url = '/bondsofart/'+art_id;
        GET(url, null, {}, {},
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError);
    }
    addBond(author_id, arts_ids, callbackSuccess, callbackError){
        var url = '/bond/'+author_id;
        var params = {
            arts_ids: arts_ids
        }
        POST(url, encodeByDict(params), {}, {
            'Content-type': 'application/x-www-form-urlencoded'
        },
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError);
    }
    removeBond(author_id, art_id, callbackSuccess, callbackError){
        var url = '/artsofauthor/'+author_id+'/'+art_id;
        POST(url, encodeByDict(params), {}, {
            'Content-type': 'application/x-www-form-urlencoded'
        },
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError);
    }
}
class AuthorsRequest{
    
    add(name, genre_id, email, burnDate, country_id, cpf, callbackSuccess, callbackError){
        var url = '/author';
        var params = {
            name:name,
            genre_id:genre_id,
            email:email,
            burnDate:burnDate,
            country_id:country_id,
            cpf:cpf
        }
        POST(url, encodeByDict(params), {}, {
            'Content-type': 'application/x-www-form-urlencoded'
        },
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError);
    }
    get(author_id=false, callbackSuccess, callbackError){
        var url = '/authors';
        if (author_id){
            url += '/'+author_id
        }
        GET(url, null, {}, {}, 
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError, null, 3, 1000);
    }
    change(author_id, name, genre_id, email, burnDate, country_id, cpf, callbackSuccess, callbackError){
        var url = '/author/'+author_id;
        var params = {
            name:name,
            genre_id:genre_id,
            email:email,
            burnDate:burnDate,
            country_id:country_id,
            cpf:cpf
        }
        PUT(url, encodeByDict(params), {}, {
            'Content-type': 'application/x-www-form-urlencoded'
        },
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError);
    }
    delete(author_id, callbackSuccess, callbackError){
        var url = '/author/'+author_id;
        DELETE(url, null, {}, {
            'Content-type': 'application/x-www-form-urlencoded'
        }, 
            function(response, options){
                callbackSuccess(JSON.parse(response), options);
            }, callbackError, null, 3, 1000);
    }
    getFromEmail(email, callbackSuccess, callbackError){
        var url = '/authors/email/'+email;
        GET(url, null, {}, {}, 
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError, null, 3, 1000);
    }
}
class ArtsRequest{
    add(name, description, publicationDate, exposureDate, callbackSuccess, callbackError){
        var url = '/art';
        var params = {
            name:name,
            description:description,
            publicationDate:publicationDate,
            exposureDate:exposureDate,
        }
        POST(url, encodeByDict(params), {}, {
            'Content-type': 'application/x-www-form-urlencoded'
        },
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError);
    }
    get(art_id=false, callbackSuccess, callbackError){
        var url = '/arts';
        if (art_id){
            url += '/'+art_id
        }
        GET(url, null, {}, {}, 
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError, null, 3, 1000);
    }
    change(art_id, name, description, publicationDate, exposureDate, callbackSuccess, callbackError){
        var url = '/art/'+art_id;
        var params = {
            name:name,
            description:description,
            publicationDate:publicationDate,
            exposureDate:exposureDate,
        }
        PUT(url, encodeByDict(params), {}, {
            'Content-type': 'application/x-www-form-urlencoded'
        },
        function(response, options){
            callbackSuccess(JSON.parse(response), options);
        }, callbackError);
    }
    delete(art_id, callbackSuccess, callbackError){
        var url = '/art/'+art_id;
        DELETE(url, null, {}, {
            'Content-type': 'application/x-www-form-urlencoded'
        }, 
            function(response, options){
                callbackSuccess(JSON.parse(response), options);
            }, callbackError, null, 3, 1000);
    }
}
selfAuthor = null
class AuthorModal{
    constructor(authorsR, countrysR, bondR){
        selfAuthor = this;
        this.authorsR = authorsR;
        this.countrysR = countrysR;
        this.bondR = bondR;
        this.infoAuthors = []
        this.infoBonds = []

        $('#authorBurndateInput').mask('00/00/0000');
        $('#authorCpfInput').mask('000.000.000-00', {reverse: true});
        
        $('#authorCpfInput').css('display', 'none');
        $('#authorCpfLabel').css('display', 'none');

        $('#authorSaveButton').click(this.saveAuthor);
        
        $('#authorCountryInput').change(this.adjustViewCpf);
            
        $('#registerAuthorButton').click(()=>{
            selfAuthor.showAddModal();
        });

        this.countrysR.get(json =>{
            json.forEach(element => {
            $("#authorCountryInput")[0].innerHTML += "<option id={0}>{1}</option>".format(element.id, element.name)
            })
        })
    }
    adjustEditBondAuthor(author_id){
        $('#authorArtsInput')[0].innerHTML = '';

        this.bondR.getBondsOfAuthor(author_id, 
        function(json){
            var listBondsIds = [];
            json.forEach((element)=>{
                listBondsIds.push(element.id);
            });
            selfArt.artsR.get(null, function(json){
                json.forEach((element, index)=>{
                    $('#authorArtsInput')[0].innerHTML += "<option id={0}>{0}&nbsp&nbsp&nbsp&nbsp{1}</option>".format(element.id, element.name.length>30?element.name.substring(30)+'...':element.name)
                    if (listBondsIds.indexOf(element.id)>-1){
                        setTimeout(()=>{$('#authorArtsInput')[0].item(index).selected = true}, 500);
                    }else{
                        $('#authorArtsInput')[0].item(index).selected = false;
                    }
                    listBondsIds
                });
            });
        })
    }
    adjustViewBondAuthor(author_id){
        $('#authorArtsInput')[0].innerHTML = '';

        this.bondR.getBondsOfAuthor(author_id, 
        function(json){
            var listBondsIds = [];
            json.forEach((element)=>{
                listBondsIds.push(element.id);
                $('#authorArtsInput')[0].innerHTML += "<option id={0}>{0}&nbsp&nbsp&nbsp&nbsp{1}</option>".format(element.id, element.name.length>30?element.name.substring(30)+'...':element.name)
            });
        })
    }
    showViewModal(author_id){
        $('#authorTextHelp').css('display', 'none');
        this.adjustViewBondAuthor(author_id);
        this.cleanHelpBlocks();
        this.infoAuthors.forEach((element)=>{
            if (element.id==author_id){
                $('#authorNameInput').val(element.name);
                $('#authorCountryInput')[0].selectedIndex = element.country_id;
                $('#authorCpfInput').val(element.cpf);
                $('#authorBurndateInput').val(element.burnDate?formatDate(element.burnDate):'');
                $('#authorGenreInput')[0].selectedIndex = element.genre_id;
                $('#authorEmailInput').val(element.email);
            }
        });
        $('#authorNameInput').attr('disabled', true);
        $('#authorCountryInput').attr('disabled', true);
        $('#authorCpfInput').attr('disabled', true);
        $('#authorBurndateInput').attr('disabled', true);
        $('#authorGenreInput').attr('disabled', true);
        $('#authorEmailInput').attr('disabled', true);
        $('#authorArtsInput').attr('disabled', true);
        $('#authorSaveButton').css('display', 'none');
        $('#authorRemoveButton').css('display', 'none');
        this.adjustViewCpf();
        $("#authorModal").modal('show');
    }
    showEditModal(author_id){
        $('#authorTextHelp').css('display', 'inline');
        this.adjustEditBondAuthor(author_id);
        this.cleanHelpBlocks();
        this.author_id_current = author_id;
        this.infoAuthors.forEach((element)=>{
            if (element.id==author_id){
                $('#authorNameInput').val(element.name);
                $('#authorCountryInput')[0].selectedIndex = element.country_id;
                $('#authorCpfInput').val(element.cpf);
                $('#authorBurndateInput').val(formatDate(element.burnDate));
                $('#authorGenreInput')[0].selectedIndex = element.genre_id;
                $('#authorEmailInput').val(element.email);
            }
        });
        $('#authorNameInput').attr('disabled', false);
        $('#authorCountryInput').attr('disabled', false);
        $('#authorCpfInput').attr('disabled', false);
        $('#authorBurndateInput').attr('disabled', false);
        $('#authorGenreInput').attr('disabled', false);
        $('#authorEmailInput').attr('disabled', false);
        $('#authorArtsInput').attr('disabled', false);
        $('#authorSaveButton').css('display', 'inline');
        $('#authorRemoveButton').css('display', 'inline');
        this.adjustViewCpf();
        $("#authorModal").modal('show');
        $('#authorNameInput').focus()
    }
    showAddModal(){
        this.adjustEditBondAuthor(-1);
        this.cleanHelpBlocks();
        this.author_id_current = '';
        $('#authorNameInput').focus()
        $('#authorNameInput').val('');
        $('#authorCountryInput')[0].selectedIndex = 0;
        $('#authorCpfInput').val('');
        $('#authorBurndateInput').val('');
        $('#authorGenreInput')[0].selectedIndex = 0;
        $('#authorEmailInput').val('');

        $('#authorNameInput').attr('disabled', false);
        $('#authorCountryInput').attr('disabled', false);
        $('#authorCpfInput').attr('disabled', false);
        $('#authorBurndateInput').attr('disabled', false);
        $('#authorGenreInput').attr('disabled', false);
        $('#authorEmailInput').attr('disabled', false);
        $('#authorArtsInput').attr('disabled', false);
        $('#authorSaveButton').css('display', 'inline');
        $('#authorRemoveButton').css('display', 'none');
        this.adjustViewCpf();
        $("#authorModal").modal('show');
        $('#authorNameInput').focus()
    }
    showAuthors(){
        var blocks = $('#blocks')[0];
        blocks.innerHTML = '';
        this.authorsR.get(false, 
        function(json){
            selfAuthor.infoAuthors = json;
            json.forEach(element => {
                blocks.innerHTML += '<div class="col-md-4 infoBox">\
                    <div class="nameBox" hidden>{0}{1}</div>\
                    <div class="card mb-4 box-shadow">\
                    <div class="card-body">\
                        <p class="card-text"><h6>{0}</h6><small>{1}</small></p>\
                        <div class="d-flex justify-content-between align-items-center">\
                        <div class="btn-group">\
                            <button type="button" class="btn btn-sm btn-outline-secondary authorViewModalButton" id={2}>Visualizar</button>\
                            <button type="button" class="btn btn-sm btn-outline-secondary authorEditModalButton" id={2}>Editar</button>\
                        </div>\
                        <small class="text-muted">{2}</small>\
                        </div>\
                    </div>\
                    </div>\
                </div>'.format(element.name, element.email, element.id);
            });
            $('.authorViewModalButton').click(function(){
                authorModal.showViewModal($(this).attr('id'));
            });
            $('.authorEditModalButton').click(function(){
                authorModal.showEditModal($(this).attr('id'));
            });
            
        }, function(response, options){
            debugger;
        })
    }
    
    cleanHelpBlocks(){
        var fields = [$('#authorNameInput'), $('#authorCountryInput'), $('#authorCpfInput'), $('#authorBurndateInput'), $('#authorGenreInput'), $('#authorEmailInput')];
        fields.forEach((field)=>{
            field.removeClass('is-valid').removeClass('is-invalid');
            
        });
    }
    checkValidEmail(email, callbackSuccess, callbackError){
        var emailValue = email;
        if (emailValue.length > 0){
            if (emailValue.indexOf("@") > -1){
                this.authorsR.getFromEmail(emailValue,
                function(json){
                    if (json.length && selfAuthor.author_id_current != json[0].id){
                        callbackSuccess(false);
                    }else{
                        callbackSuccess(true);
                    }
                }, callbackError);
            }else{
                callbackSuccess(false);
            }
        }else{
            callbackSuccess(null);
        }
        
    }
    validateForm(callback){
        selfAuthor.validFormCount = 200;
        if ($('#authorNameInput').val().length > 3){
            $('#authorNameInput').addClass('is-valid');
            $('#authorNameInput').removeClass('is-invalid');
        }else{
            $('#authorNameInput').addClass('is-invalid');
            $('#authorNameInput').removeClass('is-valid');
            selfAuthor.validFormCount -= 1;
        }

        // $('#authorGenreInput').addClass('is-valid')
        var dateValue = $('#authorBurndateInput').val()
        if (dateValue.length == 10 && formatDate(dateValue)){
            $('#authorBurndateInput').addClass('is-valid');
            $('#authorBurndateInput').removeClass('is-invalid');

        }else{
            $('#authorBurndateInput').addClass('is-invalid');
            $('#authorBurndateInput').removeClass('is-valid');
            selfAuthor.validFormCount -= 1;
        }

        var countryValue = $('#authorCountryInput')[0].selectedIndex;
        if (countryValue != 0){
            $('#authorCountryInput').addClass('is-valid');
            $('#authorCountryInput').removeClass('is-invalid');

            if (countryValue == 1){//Se pais escolhido for Brasil
                
                var cpfValue = $('#authorCpfInput').val()
                if (cpfValue.length == 14){
                    $('#authorCpfInput').addClass('is-valid');
                    $('#authorCpfInput').removeClass('is-invalid');
                }else{
                    $('#authorCpfInput').addClass('is-invalid');
                    $('#authorCpfInput').removeClass('is-valid');
                    selfAuthor.validFormCount -= 1;
                }
            }

        }else{
            $('#authorCountryInput').addClass('is-invalid');
            $('#authorCountryInput').removeClass('is-valid');
            selfAuthor.validFormCount -= 1;
        }

        this.checkValidEmail($('#authorEmailInput').val(), (isValid) =>{
            if (isValid){
                $('#authorEmailInput').addClass('is-valid');
                $('#authorEmailInput').removeClass('is-invalid');
                callback(selfAuthor.validFormCount == 200?true:false);
                
            }else if(isValid == false){
                $('#authorEmailInput').addClass('is-invalid');
                $('#authorEmailInput').removeClass('is-valid');
                callback(false);
            }else{
                // $('#authorEmailInput').addClass('is-valid');
                $('#authorEmailInput').removeClass('is-invalid');
                callback(selfAuthor.validFormCount == 200?true:false);
            }
        });
        
    }
    adjustViewCpf(){
        if ($('#authorCountryInput')[0].selectedIndex == 1){
            $('#authorCpfInput').css('display', 'inline');
            $('#authorCpfLabel').css('display', 'inline');
        }else{
            $('#authorCpfInput').css('display', 'none');
            $('#authorCpfLabel').css('display', 'none');
        }
    }
    saveAuthor(){
        selfAuthor.validateForm((isValidForm)=>{
            if (isValidForm){
                if (selfAuthor.author_id_current == ""){
                    authorsR.add($('#authorNameInput').val(), $('#authorGenreInput')[0].selectedIndex, $('#authorEmailInput').val().toLowerCase(), $('#authorBurndateInput').val().length>0?formatDate($('#authorBurndateInput').val()):'', $('#authorCountryInput')[0].selectedIndex, $('#authorCpfInput').val(), 
                    (json)=>{
                        $('#authorModal').modal('hide');
                        $('#alertModal .modal-body').text("Informações do autor salvas com sucesso!");
                        $('#alertModal').modal('show');
                        debugger
                        selfAuthor.showAuthors();
                        selfAuthor.saveBond(selfAuthor.author_id_current);
                    }, (response)=>{

                    });
                }else{
                    authorsR.change(selfAuthor.author_id_current, $('#authorNameInput').val(), $('#authorGenreInput')[0].selectedIndex, $('#authorEmailInput').val().toLowerCase(), $('#authorBurndateInput').val().length>0?formatDate($('#authorBurndateInput').val()):'', $('#authorCountryInput')[0].selectedIndex, $('#authorCpfInput').val(), 
                    (json)=>{
                        print('FORM SALVO')
                        $('#authorModal').modal('hide');
                        $('#alertModal .modal-body').text("Alterações salvas com sucesso!");
                        $('#alertModal').modal('show');
                        selfAuthor.showAuthors();
                        selfAuthor.saveBond(selfAuthor.author_id_current);
                    }, (response)=>{

                    });
                }
            }else{
                print('FORM INVALIDO')
            }
        });
    }
    saveBond(author_id){
        var chosen_arts_ids = [];
        var index = 0;
        while (index < $('#authorArtsInput')[0].length){
            var optionSelection = $('#authorArtsInput')[0].options[index].selected;
            if (optionSelection){
                chosen_arts_ids.push($('#authorArtsInput')[0].options[index].getAttribute('id'))
            }
            index += 1;
        }
        debugger;
        if (chosen_arts_ids.length > 0){
            selfAuthor.bondR.addBond(author_id, chosen_arts_ids, ()=>{}, ()=>{});
        }
    }
}
selfArt = null
class ArtModal{
    constructor(artsR, bondR){
        selfArt = this;
        this.artsR = artsR;
        this.bondR = bondR;

        $('#artDescriptionInput').keyup(()=>{
            this.showLengthDescription();
        })

        $('#artPublicationdateInput').mask('00/00/0000');
        $('#artExposuredateInput').mask('00/00/0000');

        $('#artSaveButton').click(this.saveArt);
        
        $('#registerArtButton').click(()=>{
            selfArt.showAddModal();
        });

    }
    showLengthDescription(){
        $('#artDescriptionLengthInput').text( "{0} [Até 250 Caracteres]".format( $('#artDescriptionInput').val().length) );
    }
    hideLengthDescription(){
        $('#artDescriptionLengthInput').text( "" );
    }
    adjustEditBondArt(art_id){
        $('#artAuthorsInput')[0].innerHTML = '';

        this.bondR.getBondsOfArt(art_id, 
        function(json){
            var listBondsIds = [];
            json.forEach((element)=>{
                listBondsIds.push(element.id);
            });
            selfAuthor.authorsR.get(null, function(json){
                json.forEach((element, index)=>{
                    $('#artAuthorsInput')[0].innerHTML += "<option id={0}>{0}&nbsp&nbsp&nbsp&nbsp{1}</option>".format(element.id, element.name.length>30?element.name.substring(30)+'...':element.name)
                    if (listBondsIds.indexOf(element.id)>-1){
                        setTimeout(()=>{$('#artAuthorsInput')[0].item(index).selected = true}, 500);
                    }else{
                        $('#artAuthorsInput')[0].item(index).selected = false;
                    }
                    listBondsIds
                });
            });
        })
    }
    adjustViewBondArt(art_id){
        $('#artAuthorsInput')[0].innerHTML = '';

        this.bondR.getBondsOfArt(art_id, 
        function(json){
            var listBondsIds = [];
            json.forEach((element)=>{
                listBondsIds.push(element.id);
                $('#artAuthorsInput')[0].innerHTML += "<option id={0}>{0}&nbsp&nbsp&nbsp&nbsp{1}</option>".format(element.id, element.name.length>30?element.name.substring(30)+'...':element.name)
            });
        })
    }

    showViewModal(art_id){
        $('#artTextHelp').css('display', 'none');
        this.adjustViewBondArt(art_id);
        this.cleanHelpBlocks();
        this.infoArts.forEach((element)=>{
            if (element.id==art_id){
                $('#artNameInput').val(element.name);
                $('#artDescriptionInput').val(element.description);
                $('#artPublicationdateInput').val(element.publicationDate?formatDate(element.publicationDate):'');
                $('#artExposuredateInput').val(element.exposureDate?formatDate(element.exposureDate):'');
            }
        });
        $('#artNameInput').attr('disabled', true);
        $('#artDescriptionInput').attr('disabled', true);
        $('#artPublicationdateInput').attr('disabled', true);
        $('#artExposuredateInput').attr('disabled', true);
        $('#artAuthorsInput').attr('disabled', true);

        $('#artSaveButton').css('display', 'none');
        $('#artRemoveButton').css('display', 'none');

        this.hideLengthDescription()

        $("#artModal").modal('show');
    }
    showEditModal(art_id){
        $('#artTextHelp').css('display', 'inline');
        this.adjustEditBondArt(art_id);
        this.cleanHelpBlocks();
        this.art_id_current = art_id;
        this.infoArts.forEach((element)=>{
            if (element.id==art_id){
                $('#artNameInput').val(element.name);
                $('#artDescriptionInput').val(element.description);
                $('#artPublicationdateInput').val(element.publicationDate?formatDate(element.publicationDate):'');
                $('#artExposuredateInput').val(element.exposureDate?formatDate(element.exposureDate):'');
            }
        });
        $('#artNameInput').attr('disabled', false);
        $('#artDescriptionInput').attr('disabled', false);
        $('#artPublicationdateInput').attr('disabled', false);
        $('#artExposuredateInput').attr('disabled', false);
        $('#artSaveButton').attr('disabled', false);
        $('#artAuthorsInput').attr('disabled', false);
        $('#artSaveButton').css('display', 'inline');
        $('#artRemoveButton').css('display', 'inline');
        this.showLengthDescription();

        $("#artModal").modal('show');
        $('#artNameInput').focus()
    }
    showAddModal(){
        this.adjustEditBondArt(-1);
        this.cleanHelpBlocks();
        this.art_id_current = '';

        $('#artNameInput').val('');
        $('#artDescriptionInput').val('')
        $('#artPublicationdateInput').val('');
        $('#authorBurndateInput').val('');
        $('#artExposuredateInput').val('')

        $('#artNameInput').attr('disabled', false);
        $('#artDescriptionInput').attr('disabled', false);
        $('#artPublicationdateInput').attr('disabled', false);
        $('#artExposuredateInput').attr('disabled', false);
        $('#artAuthorsInput').attr('disabled', false);
 
        $('#artAuthorsInput').attr('disabled', false);
        $('#artSaveButton').css('display', 'inline');
        $('#artRemoveButton').css('display', 'none');

        this.hideLengthDescription()

        $("#artModal").modal('show');
        $('#artNameInput').focus()
    }
    cleanHelpBlocks(){
        var fields = [$('#artNameInput'), $('#artDescriptionInput'), $('#artPublicationdateInput'), $('#artExposuredateInput')];
        fields.forEach((field)=>{
            field.removeClass('is-valid').removeClass('is-invalid');
            
        });
    }
    validateForm(callback){
        selfArt.validFormCount = 200;
        if ($('#artNameInput').val().length > 0){
            $('#artNameInput').addClass('is-valid');
            $('#artNameInput').removeClass('is-invalid');
        }else{
            $('#artNameInput').addClass('is-invalid');
            $('#artNameInput').removeClass('is-valid');
            selfArt.validFormCount -= 1;
        }
        var descriptionValue = $('#artDescriptionInput').val();
        if ( descriptionValue.length > 0 && descriptionValue.length <= 240){
            $('#artDescriptionInput').addClass('is-valid');
            $('#artDescriptionInput').removeClass('is-invalid');
        }else{
            $('#artDescriptionInput').addClass('is-invalid');
            $('#artDescriptionInput').removeClass('is-valid');
            selfArt.validFormCount -= 1;
        }
        var publicationdateValue = $('#artPublicationdateInput').val();
        var exposuredateValue = $('#artExposuredateInput').val();
        if ( publicationdateValue.length > 0 || exposuredateValue.length > 0){
            $('#artPublicationdateInput').removeClass('is-invalid');
            $('#artExposuredateInput').removeClass('is-invalid');
        }else{
            $('#artPublicationdateInput').addClass('is-invalid');
            $('#artExposuredateInput').addClass('is-invalid');
            selfArt.validFormCount -= 1;
        }
        callback(selfArt.validFormCount==200?true:false);
        
    }
    showArts(){
        var blocks = $('#blocks')[0];
        blocks.innerHTML = '';
        this.artsR.get(false, 
        function(json){
            selfArt.infoArts = json;
            json.forEach(element => {
                // debugger;
                blocks.innerHTML += '<div class="col-md-4 infoBox">\
                    <div class="nameBox" hidden>{0}{1}</div>\
                    <div class="card mb-4 box-shadow">\
                    <div class="card-body">\
                        <p class="card-text"><h6>{0}</h6><small>{1}</small></p>\
                        <div class="d-flex justify-content-between align-items-center">\
                        <div class="btn-group">\
                            <button type="button" class="btn btn-sm btn-outline-secondary artViewModalButton" id={2}>Visualizar</button>\
                            <button type="button" class="btn btn-sm btn-outline-secondary artEditModalButton" id={2}>Editar</button>\
                        </div>\
                        <small class="text-muted">{2}</small>\
                        </div>\
                    </div>\
                    </div>\
                </div>'.format(element.name, element.description, element.id);
            });
            $('.artViewModalButton').click(function(){
                artModal.showViewModal($(this).attr('id'));
            });
            $('.artEditModalButton').click(function(){
                artModal.showEditModal($(this).attr('id'));
            });
            
        }, function(response, options){
            debugger;
        })
    }
    saveArt(){
        selfArt.validateForm((isValidForm)=>{
            if (isValidForm){
                if (selfArt.art_id_current == ''){
                    artsR.add($('#artNameInput').val(), $('#artDescriptionInput').val(), $('#artPublicationdateInput').val(), $('#artExposuredateInput').val(), 
                    (json)=>{
                        $('#artModal').modal('hide');
                        $('#alertModal .modal-body').text("Informações da obra salvas com sucesso!");
                        $('#alertModal').modal('show');
                        selfArt.showArts();
                        selfArt.saveBond(selfArt.art_id_current);
                    }, (response)=>{

                    });
                }else{
                    artsR.change(selfArt.art_id_current, $('#artNameInput').val(), $('#artDescriptionInput').val(), $('#artPublicationdateInput').val().length>0?formatDate($('#artPublicationdateInput').val()):'', $('#artExposuredateInput').val().length>0?formatDate($('#artExposuredateInput').val()):'', 
                    (json)=>{
                        $('#artModal').modal('hide');
                        $('#alertModal .modal-body').text("Alterações da obra salvas com sucesso!");
                        $('#alertModal').modal('show');
                        selfArt.showArts();
                        selfArt.saveBond(selfArt.art_id_current);
                    }, (response)=>{
                    });
                }
            }else{
                print('FORM INVALIDO')
            }
        });
    }
    saveBond(art_id){
        var chosen_authors_ids = [];
        var index = 0;
        while (index < $('#artAuthorsInput')[0].length){
            var optionSelection = $('#artAuthorsInput')[0].options[index].selected;
            if (optionSelection){
                chosen_authors_ids.push($('#artAuthorsInput')[0].options[index].getAttribute('id'))
            }
            index += 1;
        }
        if (chosen_authors_ids.length > 0){
            selfArt.bondR.addBond(art_id, chosen_authors_ids, ()=>{}, ()=>{});
        }
    }
}
class Panel{
    constructor(authorModal, artModal){
        Panel.authorModal = authorModal;
        Panel.artModal = artModal;

        $('#searchInput').keyup(this.search);
        
        $('#authorOption').click(this.activeTabAuthors);
        $('#artOption').click(this.activeTabArts);
    }
    activeTabAuthors(){
        $('#authorOption').addClass('active');
        $('#artOption').removeClass('active');
        $('#searchInput').attr('placeholder', 'Procure um autor');
        Panel.authorModal.showAuthors();
    }
    activeTabArts(){
        $('#artOption').addClass('active');
        $('#authorOption').removeClass('active');
        $('#searchInput').attr('placeholder', 'Procure uma obra');
        Panel.artModal.showArts();
    }
    search(){
        //debugger
        // resultFound = false;
        var searchInput = document.getElementById("searchInput");
        document.querySelectorAll(".infoBox").forEach(elementMaster =>{
            var nameBox = elementMaster.querySelector('.nameBox');
            if (nameBox.innerText.toLowerCase().indexOf( searchInput.value.toLowerCase() ) > -1){
                elementMaster.style.display = 'inline';
                // resultFound = true;
            }else{
                elementMaster.style.display = 'none';
            }
        });
        // if (!resultFound){
        //     document.querySelector("#resultNotFound").style.display = 'inline';
        // }else{
        //     document.querySelector("#resultNotFound").style.display = 'none';
        // }
    }
}
function defineCalendar(){
    $('#dateBegin').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale: 'pt-br',
    });
    $('#dateEnd').datetimepicker({
        format: 'DD/MM/YYYY',
        daysOfWeekDisabled: [0],
        locale: 'pt-br',
        useCurrent: false //Important! See issue #1075
    });
    $("#dateBegin").on("dp.change", function (e) {
        $('#dateEnd').data("DateTimePicker").minDate(e.date);
    });
    $("#dateEnd").on("dp.change", function (e) {
        $('#dateBegin').data("DateTimePicker").maxDate(e.date);
    });
}

document.addEventListener('DOMContentLoaded', function(){
    authorsR = new AuthorsRequest();
    artsR = new ArtsRequest()
    bondR = new BondRequest()
    countrysR = new CountrysRequest();
    

    authorModal = new AuthorModal(authorsR, countrysR, bondR);
    artModal = new ArtModal(artsR, bondR)
    panel = new Panel(authorModal, artModal);
    
    authorModal.showAuthors()

    //artModal.showAddModal()

});