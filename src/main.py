# -*- coding: utf-8 -*-
__author__ = "Israel"
__date__ = "$08/02/2020 14:01:31$"
__py__ = '3.6'
import os
#os.chdir('src')
print("Ambiente: "+os.getcwd())

if os.getcwd() != "/app": #Caso o ambiente nao esteja no heroku, executa a ativasao do ambiente com dependencias
    activator = 'src/environment/scripts/activate_this.py'#Ativacao de ambiente de execucao
    with open(activator) as f:
        exec(f.read(), {'__file__': activator})

print("Ambiente: "+os.getcwd())

from flask import Flask, request, render_template
from modules.database import DbAuthor, DbArt, DbCountry, DbBond
from json import dumps, loads
#from flask import Flask, redirect, url_for, request, render_template, make_response, session, Response, flash


app = Flask(__name__, static_folder='static', static_url_path='')
app.secret_key = '123456'

@app.route('/')
def main():
    return render_template('index.html')

@app.route('/countrys', methods=['GET'])
def getCountrys():
    dbCountry = DbCountry()
    jsonCallback = dbCountry.get()

    listCountrys = []
    for info in jsonCallback['RESPONSE']:
        listCountrys.append({'id':info[0], 'name':info[1]})

    return dumps(listCountrys)

@app.route('/author', methods=['POST'])
def addAuthor():
    dbAuthor = DbAuthor()
    name = request.form['name']
    genre_id = request.form['genre_id']
    email = request.form['email']
    burnDate = request.form['burnDate']
    country_id = request.form['country_id']
    cpf = request.form['cpf']

    jsonCallback = dbAuthor.add(name, genre_id, email, burnDate, country_id, cpf)
    jsonLastRegister = dbAuthor.get(limit=1)['RESPONSE']
    
    listAuthorsFormateds = []
    for info in jsonLastRegister:
        listAuthorsFormateds.append({'id':info[0], 'name':info[1], 'genre_id':info[2], 'email':info[3], 'burnDate':info[4], 'country_id':info[5], 'cpf':info[6]})
    
    jsonCallback['RESPONSE'] = listAuthorsFormateds
    return dumps(jsonCallback)



    # jsonCallback = dbArt.add(name, description, publicationDate, exposureDate)
    # jsonCallback['RESPONSE'] = dbArt.get(limit=1)['RESPONSE']
    # return dumps(jsonCallback)

@app.route('/authors', methods=['GET'])
@app.route('/authors/<int:author_id>', methods=['GET'])
def getAuthors(author_id=None):
    dbAuthor = DbAuthor()
    jsonCallback = dbAuthor.get(author_id=author_id)
    listAuthors = []
    for info in jsonCallback['RESPONSE']:
        listAuthors.append({'id':info[0], 'name':info[1], 'genre_id':info[2], 'email':info[3], 'burnDate':info[4], 'country_id':info[5], 'cpf':info[6]})
    resultJson = dumps(listAuthors)
    return resultJson

@app.route('/author/<author_id>', methods=['PUT'])
def changeAuthor(author_id):
    dbAuthor = DbAuthor()
    #author_id = author_idrequest.form['author_id']
    name = request.form['name']
    genre_id = request.form['genre_id']
    email = request.form['email']
    burnDate = request.form['burnDate']
    country_id = request.form['country_id']
    cpf = request.form['cpf']

    jsonCallback = dbAuthor.change(author_id, name, genre_id, email, burnDate, country_id, cpf)
    return dumps(jsonCallback)

@app.route('/author/<author_id>', methods=['DELETE'])
def removeAuthor(author_id):
    dbAuthor = DbAuthor()
    jsonCallback = dbAuthor.change(author_id)
    return dumps(jsonCallback)

@app.route('/authors/email/<email>')
def getFromEmail(email):
    dbAuthor = DbAuthor()
    jsonCallback = dbAuthor.getFromEmail(email.lower())

    listAuthors = []
    for info in jsonCallback['RESPONSE']:
        listAuthors.append({'id':info[0], 'name':info[1]})
    return dumps(listAuthors)

@app.route('/art', methods=['POST'])
def addArt():
    dbArt = DbArt()
    name = request.form['name']
    description = request.form['description']
    publicationDate = request.form['publicationDate']
    exposureDate = request.form['exposureDate']

    jsonCallback = dbArt.add(name, description, publicationDate, exposureDate)
    jsonCallback['RESPONSE'] = dbArt.get(limit=1)['RESPONSE']
    return dumps(jsonCallback)

@app.route('/arts', methods=['GET'])
@app.route('/arts/<int:art_id>', methods=['GET'])
def getArts(art_id=None):
    dbArt = DbArt()
    jsonCallback = dbArt.get(art_id=art_id)

    listArts = []
    for info in jsonCallback['RESPONSE']:
        listArts.append({'id':info[0], 'name':info[1], 'description':info[2], 'publicationDate':info[3], 'exposureDate':info[4]})
    resultJson = dumps(listArts)
    return resultJson

@app.route('/art/<art_id>', methods=['PUT'])
def changeArt(art_id):
    dbArt = DbArt()
    name = request.form['name']
    description = request.form['description']
    publicationDate = request.form['publicationDate']
    exposureDate = request.form['exposureDate']

    jsonCallback = dbArt.change(art_id, name, description, publicationDate, exposureDate)
    return dumps(jsonCallback)

@app.route('/art/<art_id>', methods=['DELETE'])
def removeArt(art_id):
    dbArt = DbArt()
    jsonCallback = dbArt.change(art_id)
    return dumps(jsonCallback)

#Bonds
@app.route('/bondsofauthor/<author_id>', methods=['GET'])
def getBondsOfAuthor(author_id):
    dbBond = DbBond()
    jsonCallback = dbBond.getBondsOfAuthor(author_id)

    listArts = []
    for info in jsonCallback['RESPONSE']:
        listArts.append({'id':info[0], 'name':info[1]})
    resultJson = dumps(listArts)
    return resultJson

@app.route('/bondsofart/<art_id>', methods=['GET'])
def getBondsOfArt(art_id):
    dbBond = DbBond()
    jsonCallback = dbBond.getBondsOfArt(art_id)

    print(jsonCallback)

    listAuthors = []
    for info in jsonCallback['RESPONSE']:
        listAuthors.append({'id':info[0], 'name':info[1]})
    resultJson = dumps(listAuthors)
    return resultJson
###########################
@app.route('/bond/<author_id>', methods=['POST'])
def addBond(author_id):
    print("ARTS_IDS:")
    print (request.form['arts_ids'])
    
    dbBond = DbBond()
    jsonCallback = dbBond.addBond(author_id, request.form['arts_ids'].split(','))
    return dumps(jsonCallback)

@app.route('/bond/<author_id>/<art_id>', methods=['POST'])
def addBond_(author_id, art_id):
    dbBond = DbBond()
    jsonCallback = dbBond.addBond(author_id, art_id)
    return dumps(jsonCallback)
########################################
@app.route('/bondsofauthor/<author_id>/<art_id>', methods=['DELETE'])
def removeBondOfAuthor(author_id, art_id):
    dbBond = DbBond()
    jsonCallback = dbBond.removeBondOfAuthor(author_id, art_id)
    return dumps(jsonCallback)

@app.route('/bondsofauthor/<author_id>', methods=['DELETE'])
def removeBondsOfAuthor(author_id):
    dbBond = DbBond()
    jsonCallback = dbBond.removeBondOfAuthor(author_id, art_id)
    return dumps(jsonCallback)

@app.route('/bondofart/<art_id>', methods=['DELETE'])
def removeBondOfArt(art_id):
    dbBond = DbBond()
    jsonCallback = dbBond.removeBondOfArt(art_id)
    return dumps(jsonCallback)

port = int(os.environ.get("PORT", 5000))
app.run(host='0.0.0.0', port=port, debug=True)
